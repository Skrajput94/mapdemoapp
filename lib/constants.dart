//Font Style
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

const String customFontFamily = 'Quicksand';

TextStyle addStyleToTextWithColor(
    double textSize, FontWeight fontWeight, Color color) {
  return TextStyle(
    color: color,
    fontWeight: fontWeight,
    fontFamily: customFontFamily,
    fontSize: textSize,
// shadows: <Shadow>[CommonViewsMethods.createShadowOnText()],
  );
}

TextStyle addStyleToTextWithHexColor(
    double textSize, FontWeight fontWeight, String color) {
  return TextStyle(
    color: HexColor(color),
    fontWeight: fontWeight,
    fontFamily: customFontFamily,
    fontSize: textSize,
// shadows: <Shadow>[CommonViewsMethods.createShadowOnText()],
  );
}

getDeviceWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

getDeviceHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

getHeightSizedBox(double height) {
  return SizedBox(
    height: height,
  );
}

getWidthSizedBox(double width) {
  return SizedBox(
    width: width,
  );
}
