// @dart=2.9

import 'package:angleone/map_screen/add_new_map.dart';
import 'package:angleone/map_screen/choose_map_screen.dart';

import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return FeatureDiscovery(
      child: MaterialApp(
        title: 'Demo App', //'Angle One',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: ChooseMapScreen.screen_name,
        routes: {
          ChooseMapScreen.screen_name: (context) => ChooseMapScreen(),
          AddNewMapScreen.screen_name: (context) => AddNewMapScreen(),
        },
      ),
    );
  }
}
