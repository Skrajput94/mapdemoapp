import 'package:angleone/map_screen/choose_map_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

class ShowMapWithPinScreen extends StatefulWidget {
  static const String screen_name = "map_screen";

  double lat = 0.0;
  double lng = 0.0;
  Function(LatLong) returnLatLng;

  ShowMapWithPinScreen(this.lat, this.lng, {required this.returnLatLng});

  @override
  _ShowMapWithPinScreenState createState() => _ShowMapWithPinScreenState();
}

class _ShowMapWithPinScreenState extends State<ShowMapWithPinScreen> {
  MapController _mapController = MapController();

  void moveCamera() {
    if (_mapController != null) {
      _mapController.onReady.whenComplete(() {
        _mapController.move(LatLng(widget.lat, widget.lng), 13);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    moveCamera();

    return Container(
      child: FlutterMap(
        mapController: _mapController,
        options: MapOptions(
          onTap: (tapPosition, newLatlng) {
            setState(() {
              widget.lat = newLatlng.latitude;
              widget.lng = newLatlng.longitude;
            });
            if (widget.returnLatLng != null) {
              widget.returnLatLng(LatLong(widget.lat, widget.lng));
            }
          },
          controller: _mapController,
          center: LatLng(widget.lat, widget.lng),
          zoom: 13.0,
        ),
        layers: [
          TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c'],
            attributionBuilder: (_) {
              return Text("© Google Maps");
            },
          ),
          MarkerLayerOptions(
            markers: [
              Marker(
                width: 80.0,
                height: 80.0,
                point: LatLng(widget.lat, widget.lng),
                builder: (ctx) => Container(
                  child: Icon(
                    Icons.add_location,
                    color: Colors.red,
                    size: 50,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
