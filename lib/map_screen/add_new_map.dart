import 'dart:async';

import 'package:angleone/map_screen/choose_map_screen.dart';
import 'package:angleone/map_screen/show_map_with_pins.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class AddNewMapScreen extends StatefulWidget {
  static const String screen_name = "add_new_map_screen";

  @override
  _AddNewMapScreenState createState() => _AddNewMapScreenState();
}

class _AddNewMapScreenState extends State<AddNewMapScreen> {
  double lat = 0.0;
  double lng = 0.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCurrentLocation();
    // _determinePosition();
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  Future<void> getCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    setState(() {
      lat = position.latitude;
      lng = position.longitude;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              MaterialButton(
                color: Colors.grey,
                textColor: Colors.white,
                onPressed: () {
                  Navigator.pop(context, LatLong(lat, lng));
                },
                child: Text("Add Location"),
              ),
              TextFormField(
                initialValue: "Enter Name",
                onChanged: (name) {},
              ),
              Expanded(
                child: ShowMapWithPinScreen(
                  lat,
                  lng,
                  returnLatLng: (latLng) {
                    lat = latLng.lat;
                    lng = latLng.lng;
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
