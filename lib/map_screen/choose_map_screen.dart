import 'dart:async';

import 'package:angleone/map_screen/add_new_map.dart';
import 'package:flutter/material.dart';

import 'dialog_screen.dart';
import 'show_map_with_pins.dart';

class ChooseMapScreen extends StatefulWidget {
  static const String screen_name = "choose_map_screen";

  @override
  _ChooseMapScreenState createState() => _ChooseMapScreenState();
}

class _ChooseMapScreenState extends State<ChooseMapScreen> {
  List<LatLong> latLng = [
    LatLong(29.6069785, 78.3266826),
    LatLong(29.3786329, 78.1044199),
    LatLong(29.6069785, 78.3266826),
  ];

  final _controller = StreamController<LatLong>();

  Stream<LatLong> get stream => _controller.stream;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller.sink.add(latLng[1]);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _controller.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Row(
            children: [
              Expanded(
                flex: 3,
                child: Column(
                  children: [
                    ListView.builder(
                      padding: EdgeInsets.all(20),
                      shrinkWrap: true,
                      itemCount: latLng.length,
                      itemBuilder: (context, index) {
                        return MaterialButton(
                          color: Colors.grey[400],
                          onPressed: () {
                            _controller.sink.add(latLng[index]);
                          },
                          child: Text("Map ${index + 1}"),
                        );
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: MaterialButton(
                        color: Colors.grey[400],
                        onPressed: () async {
                          final result = await Navigator.pushNamed(
                              context, AddNewMapScreen.screen_name);
                          if (result != null) {
                            latLng.add(result as LatLong);
                            setState(() {});
                          }
                        },
                        child: Text("Add New Map"),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 7,
                child: StreamBuilder(
                  stream: stream,
                  builder: (context, AsyncSnapshot<LatLong> snapshot) {
                    if (snapshot.hasData) {
                      LatLong latLng = snapshot.data!;
                      return ShowMapWithPinScreen(
                        latLng.lat,
                        latLng.lng,
                        returnLatLng: (LatLong) {},
                      );
                    }
                    return Center(child: CircularProgressIndicator());
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  openDownloadDialog(String _downloadUrl, String fileName) {
    showDialog(context: context, builder: (context) => DialogScreen());
  }
}

class LatLong {
  double lat;
  double lng;

  LatLong(this.lat, this.lng);
}
